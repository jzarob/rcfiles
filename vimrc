set all&                        " set default config because
                                "   sys admins are jerks
" this is a dummy comment

" Use vim settings, rather than v settings.
set nocompatible

" General Configuration
set number                      " show line numbers
set backspace=indent,eol,start  " allow baskspace in insert mode
set showcmd                     " show incomplete commands
set showmode                    " show current mode
set autoread                    " reload files changed

syntax enable                   " enable syntax highlighting
set showmatch                   " show matching file types"

" Turn off Swap
set noswapfile                  " Disable swap files
set nobackup                    " remove backups
set nowb                        

" Indentation
set autoindent
set smartindent
set smarttab
set shiftwidth=4
set tabstop=4
set softtabstop=4
set expandtab

autocmd FileType make setlocal noexpandtab

set nowrap                      " Don't wrap lines
set linebreak                   " wrap at covenient spots

" Scrolling
set scrolloff=8
set sidescrolloff=15
set sidescroll=1
