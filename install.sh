mkdir -p ~/.bin 

rm -f ~/.bin/codeprint
rm -f ~/.bashrc
rm -f ~/.vimrc
rm -f ~/.gitignore
rm -f ~/.gitconfig

ln bashrc ~/.bashrc
ln vimrc ~/.vimrc
ln gitignore ~/.gitignore
ln gitconfig ~/.gitconfig
ln codeprint ~/.bin/codeprint
