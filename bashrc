export DISPLAY=:0

# Add our local bin and path to the end of the file.
export PATH=$PATH:~/.bin:.

# Set our bash history size to be 50.
HISTFILESIZE=50
HISTSIZE=50

export EDITOR=vim
export EMAIL=jazarobsky@crimson.ua.edu

# Because lazy.
alias ..='cd ..'

# What a beautiful prompt you have.
export PS1="\[\e[0;31m\]\u\[\e[m\]@\[\e[0;31m\]\h\[\e[m\]:\W\$ "

# Pretty colors for LS
export CLICOLOR=1
export LSCOLORS=GxBxhxDxfxhxhxhxhxcxcx

cdokku () {
    ssh dokku@pasoftware.us -t "$@";
}

up() {
    rsync -a $(pwd) $1:$2 > /dev/null;
}
